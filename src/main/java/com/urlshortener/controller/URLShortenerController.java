package com.urlshortener.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import com.urlshortener.domain.UrlShortener;
import com.urlshortener.domain.model.dto.UrlDto;
import com.urlshortener.domain.service.URLShortenerService;

@RestController
@RequestMapping("/shorten")
public class URLShortenerController {
	
	@Autowired
	private URLShortenerService service;

	@PostMapping
	public UrlShortener urlShortener(@RequestBody UrlDto urlDto, HttpServletRequest request)  {
		String urlLocal = request.getRequestURL().toString();
		return service.getUrlShortener(urlDto, urlLocal);
	}
	
    @RequestMapping(value = "/{id}", method=RequestMethod.GET)
    @Cacheable(value = "urlshortenercache")
    public RedirectView redirectUrl(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String redirectShortUrl = this.service.redirectShortUrl(id);
    	RedirectView redirectView = new RedirectView();
        redirectView.setUrl("http://" + redirectShortUrl);
        return redirectView;
    }
}
