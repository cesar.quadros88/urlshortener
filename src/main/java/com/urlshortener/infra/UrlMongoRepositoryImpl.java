package com.urlshortener.infra;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.urlshortener.domain.model.Url;
import com.urlshortener.domain.repository.Urlrepository;
import com.urlshortener.infra.repository.UrlRepository;

@Repository
public class UrlMongoRepositoryImpl implements Urlrepository {

	@Autowired
	private UrlRepository repository;
	
	@Override
	public Url save(Url url) {
		Url save = this.repository.save(url);
		return save;
	}

	@Override
	public Url findByUrl(String url) {
		return this.repository.findByUrl(url);
	}

	@Override
	public Url findByIdShotrUrl(String idShotrUrl) {
		return this.repository.findByIdShortUrl(idShotrUrl);
	}
}
