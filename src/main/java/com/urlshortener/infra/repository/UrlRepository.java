package com.urlshortener.infra.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.urlshortener.domain.model.Url;

public interface UrlRepository extends MongoRepository<Url, String>{
	Url findByUrl(String idShorUrl);
	Url findByIdShortUrl(String idShotrUrl);
}
