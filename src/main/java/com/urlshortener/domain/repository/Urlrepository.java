package com.urlshortener.domain.repository;

import org.springframework.stereotype.Repository;

import com.urlshortener.domain.model.Url;

@Repository
public interface Urlrepository {
	public Url save(Url url);
	public Url findByUrl(String idShortUrl);
	public Url findByIdShotrUrl(String idShotrUrl);
}
