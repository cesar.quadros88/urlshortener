package com.urlshortener.domain;

public class UrlShortener {

	private String urlShortener;
	
	public UrlShortener(String urlShortener) {
		super();
		this.urlShortener = urlShortener;
	}

	public String getUrlShortener() {
		return urlShortener;
	}

	public void setUrlShortener(String urlShortener) {
		this.urlShortener = urlShortener;
	}
}
