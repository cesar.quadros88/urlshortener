package com.urlshortener.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.urlshortener.domain.UrlShortener;
import com.urlshortener.domain.model.Url;
import com.urlshortener.domain.model.dto.UrlDto;
import com.urlshortener.domain.repository.Urlrepository;
import com.urlshortener.domain.utils.UUIDUtils;

@Service
public class URLShortenerService {
	
	@Autowired
	private Urlrepository repository;
	
	@Autowired
	private UUIDUtils uuid;
	
	public UrlShortener getUrlShortener(UrlDto urlDto, String urlLocal) {
		String generateId = uuid.generateId();
		Url findByIdShortUrl = repository.findByUrl(urlDto.getUrl());
		if(!ObjectUtils.isEmpty(findByIdShortUrl)) {
			return new UrlShortener(builderShortenerUrl(urlLocal, findByIdShortUrl.getIdShotrUrl()));
		}
		save(generateId, urlDto);
		return new UrlShortener(builderShortenerUrl(urlLocal, generateId));
	}
	
	public String redirectShortUrl(String id) throws Exception {
		Url findByIdShotrUrl = repository.findByIdShotrUrl(id);
		if(ObjectUtils.isEmpty(findByIdShotrUrl)) {
			throw new Exception("URL Inexistente");
		}
		return findByIdShotrUrl.getUrl();
	}
	
	private void save(String generateId, UrlDto urlDto) {
		String urlFinal = urlDto.getUrl().replaceAll("http://", "").replaceAll("https://", "");
		Url url = new Url(generateId, urlFinal);
		repository.save(url);
	}
	
	public String takeOut(String url) {
		return url.replaceAll("http://", "").replaceAll("https://", "");
	}
	
	private String builderShortenerUrl(String urlLocal, String id) {
		return urlLocal.concat("/").concat(id);
	}
}
