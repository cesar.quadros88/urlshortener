package com.urlshortener.domain.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Url {

	@Id
	private String id;
	private String idShortUrl;
	private String url;
	
	public Url(String idShortUrl, String url) {
		super();
		this.idShortUrl = idShortUrl;
		this.url = url;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdShotrUrl() {
		return idShortUrl;
	}
	public void setIdShortUrl(String idShortUrl) {
		this.idShortUrl = idShortUrl;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
