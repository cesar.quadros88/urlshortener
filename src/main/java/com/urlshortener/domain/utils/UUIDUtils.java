package com.urlshortener.domain.utils;

import java.util.UUID;

import org.springframework.stereotype.Service;

@Service
public class UUIDUtils {

	public String generateId() {
		String longId = UUID.randomUUID().toString();
		return longId.substring(0, 5);
	}
}
