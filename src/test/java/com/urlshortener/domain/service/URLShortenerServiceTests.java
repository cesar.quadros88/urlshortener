package com.urlshortener.domain.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.urlshortener.domain.UrlShortener;
import com.urlshortener.domain.model.Url;
import com.urlshortener.domain.model.dto.UrlDto;
import com.urlshortener.domain.repository.Urlrepository;
import com.urlshortener.domain.utils.UUIDUtils;

@TestInstance (Lifecycle.PER_CLASS)
@SpringBootTest
class URLShortenerServiceTests {

	@InjectMocks
	private URLShortenerService uRLShortenerService;

	@Mock
	private Urlrepository repository;
	@Mock
	private UUIDUtils uuid;

	private UrlDto urlDto;
	private Url url;
	private String urlLocal;
	private String urlLong;
	private String idUrlShort;

	@BeforeAll
	public void init() {
		urlDto = new UrlDto();
		urlLong = "www.google.com";
		urlDto.setUrl(urlLong);
		url = new Url("1234", urlLong);
		urlLocal = "localhost:8080/shortener";
		idUrlShort = "1234";
	}

	@Test
	public void getUrlShortenerTestOK() {
		when(uuid.generateId()).thenReturn("1234");
		UrlShortener urlShortener = uRLShortenerService.getUrlShortener(urlDto, urlLocal);
		assertEquals(urlLocal+"/1234", urlShortener.getUrlShortener());
	}
	
	@Test
	public void redirectShortUrlTestOK() throws Exception {
		when(repository.findByIdShotrUrl(idUrlShort)).thenReturn(url);
		String redirectShortUrl = uRLShortenerService.redirectShortUrl(idUrlShort);
		assertEquals(urlLong, redirectShortUrl);
	}
}
