# O que é:
Aplicação com objetivo de encurtar URL's


<br>

# Técnologias utilizadas:
Java EE, SpringBoot, Spring Data, Spring Cache, JUnit, Mockito


<br>

# Como Rodar o projeto:
### Pré requisitos:
    Necessário ter instalado: Docker, Docker Compose e Git 

### 1 - Baixar o projeto:
    https://gitlab.com/cesar.quadros88/urlshortener.git
    
### 2 - Rodar o projeto:
    Dentro do diretorio do projeto execute: docker-compose up -d
    Para confirmar se os containers estão rodando, digite no console: docker ps
    Deverá retornar:
    
    CONTAINER ID IMAGE                       COMMAND                  CREATED     STATUS         PORTS                    NAMES
    414fc962ad69 cesarquadros/urlshortener   "java -jar URLShorte…"   6 days ago  Up 14 seconds  0.0.0.0:8080->8080/tcp   servico-urlshortener
    2452b386915d mongo                       "docker-entrypoint.s…"   6 days ago  Up 37 seconds  27017/tcp                mongo-urlshortener

    
<br>    


# Como utilizar o serviço:
### Gerar a URL encurtada
    URL: localhost:8080/shorten
    Metodo: POST
    Request body: 
    {
        "url": "www.google.com.br"
    }
    Response body: 
    {
        "urlShortener": "http://localhost:8080/shorten/a061d"
    }
    
### Utilizar URL encurtada
    Cole no novegador a url retornada no serviço anterior
    
    URL: http://localhost:8080/shorten/{idUrlEncurtada} - Exemplo: http://localhost:8080/shorten/a061d
    Metodo: GET
    Response: Você será redirecionado para url original
    
    
    
    
    
    